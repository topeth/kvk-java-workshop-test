import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class ExceptionsWorkshop {

    void fileNotFound() throws FileNotFoundException {

            File file = new File("E://file.txt");
            FileReader fr = new FileReader(file);

    }

    void arrayOutOfBound() {
        int num[] = {1, 2, 3, 4};
        System.out.println(num[5]);
    }

    void findallyTest() {
        int a[] = new int[2];
        try {
            System.out.println("Access element three :" + a[3]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Exception thrown  :" + e);
        } finally {
            a[0] = 6;
            System.out.println("First element value: " + a[0]);
            System.out.println("The finally statement is executed");
        }
    }

    public static void main(String[] args) {
        ExceptionsWorkshop exceptionsWorkshop = new ExceptionsWorkshop();

        //exceptionsWorkshop.fileNotFound();
        //exceptionsWorkshop.arrayOutOfBound();
        exceptionsWorkshop.findallyTest();
    }
}

