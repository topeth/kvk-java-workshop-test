import java.util.Arrays;
import java.util.List;

public class FunctionalProgramming {
    void show_array_list_normal(){
        List<Integer> numbers
                = Arrays.asList(11, 22, 33, 44,
                55, 66, 77, 88,
                99, 100);

        // External iterator, for Each loop
        for (Integer n : numbers) {
            System.out.print(n + " ");
        }
        System.out.println();
    }
    void show_array_list_fp(){
        List<Integer> numbers
                = Arrays.asList(11, 22, 33, 44,
                55, 66, 77, 88,
                99, 100);

        // Internal iterator
        numbers.forEach(number
                -> System.out.print(
                number + " "));
        System.out.println();
    }

    void find_sum_normal(){
        List<Integer> numbers
                = Arrays.asList(11, 22, 33, 44,
                55, 66, 77, 88,
                99, 100);

        int result = 0;
        for (Integer n : numbers) {
            if (n % 2 == 0) {
                result += n * 2;
            }
        }
        System.out.println(result);
    }

    void find_sum_streaming(){
        List<Integer> numbers
                = Arrays.asList(11, 22, 33, 44,
                55, 66, 77, 88,
                99, 100);
        System.out.println(
                numbers.stream()
                        .filter(number -> number % 2 == 0)
                        .mapToInt(e -> e * 2)
                        .sum());
        System.out.println();
    }

    public static void main(String[] args)
    {
        FunctionalProgramming fp = new FunctionalProgramming();
        fp.show_array_list_normal();
        fp.show_array_list_fp();
        fp.find_sum_normal();
        fp.find_sum_streaming();
    }
}
